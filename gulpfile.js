// *************************************
//
//   Gulpfile
//
// *************************************

// -------------------------------------
//   Tâches
// -------------------------------------
// `gulp`
// `dev:styles`        : Injection des dépendances Bower (SCSS). Compilation SASS & autoprefix CSS + Source maps
// `dev:scripts`       : Injection des dépendances Bower (JS) dans les fichiers html.
// `dev:fonts`         : Récupération des fonts des composants Bower
// `prod:useref`       : Concaténation, minification des fichiers css et js + versioning
// `prod:fonts`        : Copie des fonts dans le répertoire de production
// `prod:images`       : Optimisation des images et copie dans le répertoire de production
// `browserSync:init`  : Initialise une instance de browsersync
// `browserSync:reload`: Actualise du navigateur
// `clearCache`        : Raz du cache généré par le plugin gulp-cache
// `clean`             : Supprime le contenu du répertoire de production
// `build`             : dev vers dist => clean | dev:styles, dev:scripts, dev:fonts | prod:useref, prod:images, prod:fonts
// `watch`             : Surveillance (browser refresh) => styles (dev:styles), scripts, html, img
// `default`           : dev:styles, dev:scripts, dev:fonts | browserSync:init, watch
// *************************************

// -------------------------------------
//   Modules
// -------------------------------------
//
// gulp              : The streaming build system
// gulp-load-plugins : Chargement auto des plugins
// gulp-sass         : Compilation SASS
// gulp-sourcemaps   : Debug des styles en scss (et non css) dans les outils de développement du navigateur
// gulp-autoprefixer : Prefix CSS
// gulp-clean-css    : Minify CSS
// main-bower-files  : Récupération des principaux fichiers des dépendances bower
// gulp-imagemin     : Compression des images
// gulp-cache        : Mémorisation des fichiers déjà traités
// gulp-useref       : Concatenation des css ou scripts déclarés dans les fichiers html
// wiredep           : injection des dépendances bower (scss, js, css ...)
// gulp-uglify       : Minify JS
// gulp-if           : Exécution conditionnelle
// del               : Suppression de fichiers et de répertoires
// gulp-rev          : Versionne les fichiers js et css (hash en fin de nom de fichier)
// gulp-rev-replace  : Réécriture des noms de fichiers générés avec gulp-rev
// browser-sync      : Actualisation, synchronisation Web selon modifications css, html ...
// -------------------------------------

// -------------------------------------
//   Requires
// -------------------------------------
var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var mainBowerFiles = require('main-bower-files');
var del = require('del');
var browserSync = require('browser-sync').create();
var wiredep = require('wiredep').stream;

// -------------------------------------
//   Config
// -------------------------------------
// Paths
var basePaths = {
    src: './public_html/',
    dest: './dist/'
};
var assetsFolder = 'assets/';
var paths = {
    scss: {
        src: basePaths.src + assetsFolder + 'scss/'
    },
    css: {
        src: basePaths.src + assetsFolder + 'css/',
        dest: basePaths.dest + assetsFolder + 'css/'
    },
    scripts: {
        src: basePaths.src + assetsFolder + 'js/',
        dest: basePaths.dest + assetsFolder + 'js/'
    },
    fonts: {
        src: basePaths.src + assetsFolder + 'fonts/',
        dest: basePaths.dest + assetsFolder + 'fonts/'
    },
    images: {
        src: basePaths.src + assetsFolder + 'img/',
        dest: basePaths.dest + assetsFolder + 'img/'
    }
};
var devFiles = {
    scss: paths.scss.src + '**/*.scss',
    html: basePaths.src + '**/*.html',
    fonts: paths.fonts.src + '**/*.{otf,eot,svg,ttf,woff,woff2}',
    images: paths.images.src + '**/*.{png,jpg,jpeg,gif}',
    scripts: paths.scripts.src + '**/*.js'
};
// Browser
var browsers = 'Chrome';


// -------------------------------------
//   Task: dev:styles
// -------------------------------------
// Phase de développement
// -------------------------------------
// => injection des dépendances bower (@import ....scss) (wiredep)
// => compilation sass to css (gulp-sass)
// => autoprefixe css (gulp-autoprefixer)
// => création de main.css
// -------------------------------------
gulp.task('dev:styles', function () {

    return gulp.src(devFiles.scss)
            // inject sass dependances
            .pipe(wiredep({
                dependencies: false,
                devDependencies: true,
                includeSelf: false
            }))
            // write sourcemaps (browser scss debug)
            .pipe(plugins.sourcemaps.init())
                // compile sass
                .pipe(plugins.sass().on('error', plugins.sass.logError))
                // prefixe css
                .pipe(plugins.autoprefixer())
            .pipe(plugins.sourcemaps.write('./maps'))
            .pipe(gulp.dest(paths.css.src));
});

// -------------------------------------
//   Task: dev:scripts
// -------------------------------------
// Phase de développement
// -------------------------------------
// => injection des dépendances bower
// dans les fichiers *.html(wiredep)
// -------------------------------------
gulp.task('dev:scripts', function () {
    return gulp.src(devFiles.html)
            .pipe(wiredep({
                dependencies: false,
                devDependencies: true,
                includeSelf: false
            }))
            .pipe(gulp.dest(basePaths.src));
});


// -------------------------------------
//   Task: dev:fonts
// -------------------------------------
// Phase de développement
// -------------------------------------
gulp.task('dev:fonts', function (cb) {
    var bowerFontsFiles = mainBowerFiles({
        includeDev: true,
        filter: '**/*.{otf,eot,svg,ttf,woff,woff2}'
    });
    // check for glob empty array
    if (bowerFontsFiles.length === 0) {
        cb();
        return;
    }

    return gulp.src(bowerFontsFiles, {allowEmpty: true})
            .pipe(gulp.dest(paths.fonts.src));
});

// -------------------------------------
//   Task: prod:useref
// -------------------------------------
// Phase de prod
// -------------------------------------
gulp.task('prod:useref', function () {
    return gulp
            .src(devFiles.html)
            .pipe(plugins.useref())
            .pipe(plugins.if('*.js', plugins.uglify()))
            .pipe(plugins.if('*.js', plugins.rev()))
            .pipe(plugins.if('*.css', plugins.cleanCss()))
            .pipe(plugins.if('*.css', plugins.rev()))
            .pipe(plugins.revReplace({replaceInExtensions: ['.html']}))
            .pipe(gulp.dest(basePaths.dest))
            .pipe(plugins.rev.manifest('rev-manifest.json', {merge: true}))
            .pipe(gulp.dest(basePaths.dest + assetsFolder));
});

// -------------------------------------
//   Task: prod:fonts
// -------------------------------------
// Phase de prod
// -------------------------------------
gulp.task('prod:fonts', function () {
    return gulp
            .src(devFiles.fonts)
            .pipe(gulp.dest(paths.fonts.dest));
});

// -------------------------------------
//   Task: prod:images
// -------------------------------------
// Phase de prod
// -------------------------------------
gulp.task('prod:images', function () {
    return gulp.src(devFiles.images)
            .pipe(plugins.cache(
                plugins.imagemin({
                    verbose: true
                })
            ))
            .pipe(gulp.dest(paths.images.dest));
});


// -------------------------------------
//   Task: browserSync:init
// -------------------------------------
gulp.task('browserSync:init', function () {
    browserSync.init({
        server: {
            baseDir: basePaths.src
        },
        browser: browsers,
        notify: false
    });
});

// -------------------------------------
//   Task: browserSync:reload
// -------------------------------------
gulp.task('browserSync:reload', function (done) {
    browserSync.reload();
    done();
});

// -------------------------------------
//   Task: clearCache
// -------------------------------------
gulp.task('clearCache', function (done) {
    return plugins.cache.clearAll(done);
});

// -------------------------------------
//   Task: clean
// -------------------------------------
gulp.task('clean', function (done) {
    del.sync('dist/**/*', done());
});

// -------------------------------------
//   Task: build
// -------------------------------------
// Compiler le développement pour mise en prod (dist)
// -------------------------------------
gulp.task('build', gulp.series(
        'clean', 
        gulp.parallel('dev:styles', 'dev:scripts', 'dev:fonts'),
        gulp.parallel('prod:useref', 'prod:images', 'prod:fonts')
));

// -------------------------------------
//   Task: watch
// -------------------------------------
gulp.task('watch', function () {
    // styles
    gulp.watch(devFiles.scss, gulp.series('dev:styles', 'browserSync:reload'));
    // js
    gulp.watch(devFiles.scripts, gulp.parallel('browserSync:reload'));
    // html
    gulp.watch(devFiles.html, gulp.parallel('browserSync:reload'));
    // img
    gulp.watch(devFiles.images, gulp.parallel('browserSync:reload'));
});

// -------------------------------------
//   Task: default
// -------------------------------------
gulp.task('default', gulp.series(
        gulp.parallel('dev:styles', 'dev:scripts', 'dev:fonts'),
        gulp.parallel('browserSync:init', 'watch')
));

