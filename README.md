# Gulp avec Netbeans 8.2 (Windows)


## Sommaire :

  * [Installation de Gulp et paramétrage de Netbeans](howto/01_installation.md)
  * [Automatisation de tâches dans un projet HTML Netbeans](howto/02_automatisation_html_project.md)