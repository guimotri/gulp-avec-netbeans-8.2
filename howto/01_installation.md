# Gulp 4 avec Netbeans 8.2 (Windows)
## Installation de Gulp et paramétrage de Netbeans

### 1. Installer Gulp 4

* Préalable
    
    * Installer [Node.js](https://nodejs.org) (utiliser la version Current)
        
        * Node.js est installé par défaut dans "C:\Program Files\nodejs\"
        * Le gestionnaire de paquet Npm est ajouté à la variable d'environnement Windows "Path" (C:\Users\user\AppData\Roaming\npm)

    * Tester Node (cmd.exe)
        
        ```
        node -v
        ```

        => doit retourner la version de Node (7.5.0 pour moi)
        
    * Tester Npm (cmd.exe)
        
        ```
        npm -v
        ```

        => doit retourner la version de Npm (4.4.4 pour moi)
        
    * Redémarrer Windows
    
* Installation globale (systéme) de [Gulp](http://gulpjs.com/)
    
    * Exécuter la commande (cmd.exe)
        
        ```
        npm install -g gulp-cli
        ```

    * Tester Gulp (cmd.exe)
        
        ```
        gulp -v
        ```

        => doit retourner la version de Gulp (CLI 1.2.2 pour moi)
        
### 2. Parametrer Node et Npm dans NETBEANS

* Accéder à la fenêtre des options Node.js (Menu: Outils>Options, Rubrique : HTML/JS, Onglet: Node.js)

    ![Netbeans node options](../img/01_Netbeans_opt_node.jpg)

* Renseigner le path de Node (browse ou search) et récupérer le sources (download)
* Renseigner le path de Npm (browse ou search)
* Valider

### 3. Parametrer Gulp et Npm dans NETBEANS

* Accéder à la fenêtre des options Gulp (Menu: Outils>Options, Rubrique : HTML/JS, Onglet: Gulp)

    ![netbeans gulp options](../img/02_Netbeans_opt_gulp.jpg)

* Renseigner le path de Gulp (browse ou search)
* Valider

### 4. Initialisation du projet Netbeans

* Création du fichier package.json :

    Exécuter la commande (cmd.exe) à la racine du projet
        
        npm init
    
* installer le plugin Gulp (Local version 4.0.0-alpha.2) :

    Exécuter la commande (cmd.exe) à la racine du projet
        
        npm install gulpjs/gulp#4.0 --save-dev
        
    Le plugin Gulp sera inscrit dans les dépendances de développement du fichier package.json :

        {
          "name": "gulp_essais2",
          "version": "0.0.1",
          "description": "test gulp avec netbeans 8.2",
          "main": "index.js",
          "directories": {
            "test": "test"
          },
          "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
          },
          "keywords": [
            "test",
            "gulp",
            "netbeans"
          ],
          "author": "guimo",
          "license": "ISC",
          "devDependencies": {
            "gulp": "github:gulpjs/gulp#4.0"
          },
          "dependencies": {},
          "optionalDependencies": {}
        }

* Création du fichier gulpfile.js :

    Contenu de base :

        // Requires
        var gulp = require('gulp');

        //Tâche default
        gulp.task('default', function () {
            // place code for your default task here
        });
