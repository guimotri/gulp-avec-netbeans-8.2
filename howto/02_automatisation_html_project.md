# Gulp 4 avec Netbeans 8.2 (Windows)
## Automatisation de tâches dans un projet HTML Netbeans


### 1. Prérequis

[Installation de Gulp et paramétrage de Netbeans](01_installation.md)

### 2. Objectifs

Phase de développement - Automatiser les tâches courantes suivantes :

* **STYLES**

    * Injection des dépendances bower dans les fichiers .scss
    * Compilation SASS
    * Prefixes CSS


* **FONTS**

    * Récupération des fonts dans les composants Bower

* **SCRIPTS**

    * Injection des dépendances js dans les fichiers html


* **SURVEILLANCE**

    * Fichiers SASS (Rafraichissement du navigateur + compilation)
    * Fichiers HTML (Rafraichissement du navigateur)
    * Fichiers JS (Rafraichissement du navigateur)
    * Fichiers Images (Rafraichissement du navigateur)

Phase de production - Automatiser les tâches courantes suivantes :

* **CSS**

    * Concaténation, minification et versioning des fichiers de styles dans le répertoire de prod

* **JS**

    * Concaténation, minification et versioning des fichiers de scripts dans le répertoire de prod

* **FONTS**

    * Copie des fichiers dans le répertoire de prod

* **HTML**

    * Copie des fichiers dans le répertoire de prod

* **IMAGES**

    * Compression des images  dans le répertoire de prod
    * Mise en cache des compressions pour ne pas répéter


### 3. Structure du projet

* **Arborescence de développement (source avant init gulp)**

![netbeans gulp options](../img/04_Netbeans_projet_arbo1.jpg)

* **Arborescence de développement (source après init gulp)**

![netbeans gulp options](../img/04_Netbeans_projet_arbo2.jpg)

* **Arborescence de production (distant après compilation)**

![netbeans gulp options](../img/04_Netbeans_projet_arbo3.jpg)

### 4. Installation des plugins nécessaires à l'éxécution des tâches

* **Liste des plugins**


|Catégorie     |Plugin            |Description                                                  |
|:------------:|:----------------:|-------------------------------------------------------------|
|Tâches        |gulp              |Gestion de tâches dans le projet                             |
|Plugins       |gulp-load-plugins |Chargement des plugins Gulp de package.json (prefix "gulp-") |
|SASS          |gulp-sass         |Compilation SASS                                             |
|SASS          |gulp-sourcemaps   |Debug des styles en scss dans le navigateur                  |
|CSS           |gulp-autoprefixer |Ajout des prefixes CSS                                       |
|CSS           |gulp-clean-css    |Minification des fichiers CSS                                |
|BOWER         |main-bower-files  |Récupération des principaux assets des composants BOWER      |
|IMG           |gulp-imagemin     |Compression des images (jpg, png, gif)                       |
|Cache         |gulp-cache        |Ne pas retraiter les fichiers non modifiés                   |
|HTML          |gulp-useref       |Concat des css ou scripts déclarés dans les fichiers html    |
|BOWER         |wiredep           |injection des dépendances bower (scss, js, css ...)          |
|JS            |gulp-uglify       |Minification des fichiers js                                 |
|Tâches        |gulp-if           |Exécution conditionnelle                                     |
|Fichiers      |del               |Suppression de fichiers et de répertoires                    |
|Versioning    |gulp-rev          |Versionne les fichiers js et css (hash dans le nom de fichier)|
|Versioning    |gulp-rev-replace  |Réécriture des déclarations de scripts et styles dans les fichiers html selon hash générés avec gulp-rev |
|Tests         |browser-sync      |Actualisation, synchronisation Web selon modifications css, html ...|


* **Installation**

    Exécuter la commande (cmd.exe) à la racine du projet :

        npm install --save-dev gulp gulp-load-plugins gulp-sass gulp-sourcemaps gulp-autoprefixer gulp-clean-css main-bower-files gulp-imagemin gulp-cache gulp-useref wiredep gulp-uglify gulp-if del gulp-rev gulp-rev-replace browser-sync

    Les dépendances sont inscrites dans **package.json** :
    
    *./package.json*
    
        {
          "name": "netbeans_gulp",
          "version": "0.0.1",
          "description": "test netbeans 8.2 avec gulp",
          "main": "index.js",
          "directories": {
            "test": "test"
          },
          "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
          },
          "keywords": [
            "test",
            "gulp",
            "netbeans"
          ],
          "author": "guimo",
          "license": "ISC",
          "devDependencies": {
            "browser-sync": "^2.18.8",
            "del": "^2.2.2",
            "gulp": "github:gulpjs/gulp#4.0",
            "gulp-autoprefixer": "^3.1.1",
            "gulp-cache": "^0.4.6",
            "gulp-clean-css": "^3.0.4",
            "gulp-if": "^2.0.2",
            "gulp-imagemin": "^3.2.0",
            "gulp-load-plugins": "^1.5.0",
            "gulp-rev": "^7.1.2",
            "gulp-rev-replace": "^0.4.3",
            "gulp-sass": "^3.1.0",
            "gulp-sourcemaps": "^2.5.1",
            "gulp-uglify": "^2.1.2",
            "gulp-useref": "^3.1.2",
            "main-bower-files": "^2.13.1",
            "wiredep": "^4.0.0"
          },
          "dependencies": {},
          "optionalDependencies": {}
        }


### 5. Création des tâches dans le fichier gulpfile.js

* **Liste des tâches Gulp**

|Nom de la tâche   |Description                                                                             |
|:----------------:|----------------------------------------------------------------------------------------|
|dev:styles        |Injection des dépendances Bower (SCSS). Compilation SASS & autoprefix CSS + Source maps |
|dev:scripts       |Injection des dépendances Bower (JS) dans les fichiers html                             |
|dev:fonts         |Récupération des fonts des composants Bower                                             |
|prod:useref       |Concaténation, minification des fichiers css et js + versioning                         |
|prod:fonts        |Copie des fonts dans le répertoire de production                                        |
|prod:images       |Optimisation des images et copie dans le répertoire de production                       |
|browserSync:init  |Initialise une instance de browsersync                                                  |
|browserSync:reload|Actualise du navigateur                                                                 |
|clearCache        |Raz du cache généré par le plugin gulp-cache                                            |
|clean             |Supprime le contenu du répertoire de production                                         |
|build             |clean + (dev:styles, dev:scripts, dev:fonts) + (prod:useref, prod:images, prod:fonts)   |
|watch             |Surveillance fichiers (browser refresh) => styles (compilation) , scripts, html, img    |
|default           |dev:styles, dev:scripts, dev:fonts + browserSync:init, watch                            |


* **Déclaration et chargement des plugins**
    
    *./gulp.js*
    
        var gulp = require('gulp');
        var plugins = require('gulp-load-plugins')();
        var mainBowerFiles = require('main-bower-files');
        var del = require('del');
        var browserSync = require('browser-sync').create();
        var wiredep = require('wiredep').stream;

    * La variable **plugins** :
    
    Les plugins préfixés "gulp-" seront automatiquement chargés via le plugin "gulp-load-plugin". On pourra les utiliser comme suit : ``plugins.sass()``
    
    * La variable **mainBowerFiles** :
    
    Analyse la proriété "main" du fichier "bower.json" de chacune des dépendances Bower du projet et retourne la liste des principaux fichiers (js, sass, less, css, fonts ...) dans un array.
    
    Certains composants Bower ne listent pas toujours correctement les principaux fichiers dans leur bower.json . Dans ce cas, il est nécessaire de faire un override dans le fichier bower.json du projet.
    
    Exemple concret avec le composant font-awesome qui ne déclare pas les fonts:
    
    *./public_html/bower_components/font-awesome/bower.json*
    
        {
          "name": "font-awesome",
          "description": "Font Awesome",
          "keywords": [],
          "homepage": "http://fontawesome.io",
          "dependencies": {},
          "devDependencies": {},
          "license": ["OFL-1.1", "MIT", "CC-BY-3.0"],
          "main": [
            "less/font-awesome.less",
            "scss/font-awesome.scss"
          ],
          "ignore": [
            "*/.*",
            "*.json",
            "src",
            "*.yml",
            "Gemfile",
            "Gemfile.lock",
            "*.md"
          ]
        }
    
    Overrides dans le projet :
    
    *./bower.json*
    
            "overrides": {
                "font-awesome": {
                  "main": [
                    "scss/font-awesome.scss",
                    "fonts/FontAwesome.otf",
                    "fonts/fontawesome-webfont.eot",
                    "fonts/fontawesome-webfont.svg",
                    "fonts/fontawesome-webfont.ttf",
                    "fonts/fontawesome-webfont.woff",
                    "fonts/fontawesome-webfont.woff2"
                  ]
                }
            }

* **Déclaration des paramètres**

    *./gulp.js*
    
            // Paths
            var basePaths = {
                src: './public_html/',
                dest: './dist/'
            };
            var assetsFolder = 'assets/';
            var paths = {
                scss: {
                    src: basePaths.src + assetsFolder + 'scss/'
                },
                css: {
                    src: basePaths.src + assetsFolder + 'css/',
                    dest: basePaths.dest + assetsFolder + 'css/'
                },
                scripts: {
                    src: basePaths.src + assetsFolder + 'js/',
                    dest: basePaths.dest + assetsFolder + 'js/'
                },
                fonts: {
                    src: basePaths.src + assetsFolder + 'fonts/',
                    dest: basePaths.dest + assetsFolder + 'fonts/'
                },
                images: {
                    src: basePaths.src + assetsFolder + 'img/',
                    dest: basePaths.dest + assetsFolder + 'img/'
                }
            };
            var devFiles = {
                scss: paths.scss.src + '**/*.scss',
                html: basePaths.src + '**/*.html',
                fonts: paths.fonts.src + '**/*.{otf,eot,svg,ttf,woff,woff2}',
                images: paths.images.src + '**/*.{png,jpg,jpeg,gif}',
                scripts: paths.scripts.src + '**/*.js'
            };
            // Browser
            var browsers = 'Chrome';
    
    La variable **basePaths** définit les répertoires de développement ``basePaths.src`` et de production ``basePaths.dest`` .
    
    La variable **assetsFolder** correspond au nom du répertoire principal des assets.
    
    La variable **paths** définit les répertoires des assets en développement et production.
    
    La variable **devFiles** définit les différents fichiers en phase de développement.
    
    La variable **browsers** définit le(s) navigateurs avec le(s)quel(s) sera testée l'application.


* **Tâche "dev:styles"**

    *./gulp.js*
    
        gulp.task('dev:styles', function () {

            return gulp.src(devFiles.scss)
                    // inject sass dependances
                    .pipe(wiredep({
                        dependencies: false,
                        devDependencies: true,
                        includeSelf: false
                    }))
                    // write sourcemaps (browser scss debug)
                    .pipe(plugins.sourcemaps.init())
                        // compile sass
                        .pipe(plugins.sass().on('error', plugins.sass.logError))
                        // prefixe css
                        .pipe(plugins.autoprefixer())
                    .pipe(plugins.sourcemaps.write('./maps'))
                    .pipe(gulp.dest(paths.css.src));
        });
    
    * Description des actions :
        
        - Injection des fichiers scss des dépendances Bower (plugin wiredep) dans le fichier "main.scss"
        
        Bloc scss dans le fichier main.scss pour injection :
        
        *./public_html/assets/scss/main.scss*
        
                // bower:scss
                // endbower
        
        Bloc scss dans le fichier main.scss après injection :
        
        *./public_html/assets/scss/main.scss*
        
                // bower:scss
                @import "../../bower_components/font-awesome/scss/font-awesome.scss";
                // endbower
        
        - Compilation du fichier "main.scss" (plugin gulp-sass)
        - Enregistrement d'un fichier source map (main.css.map) dans le répertoire css source qui permettra de débugger les styles au format scss dans le navigateur
        - Autprefixe des propriétés CSS
        - Génération du fichier main.css dans le répertoire css source
    

* **Tâche "dev:scripts"**

    *./gulp.js*
    
        gulp.task('dev:scripts', function () {
            return gulp.src(devFiles.html)
                    .pipe(wiredep({
                        dependencies: false,
                        devDependencies: true,
                        includeSelf: false
                    }))
                    .pipe(gulp.dest(basePaths.src));
        });
    
    * Description des actions :
        
        - Injection des fichiers js des dépendances Bower (plugin wiredep) dans le fichier html où sont déclarés les scripts
        
        Bloc bower:js dans le fichier index.html pour injection :
        
        *./public_html/index.html*
        
                    <!-- build:js assets/js/main.min.js -->
                    <!-- bower:js -->
                    <!-- endbower -->
                    <script src="assets/js/main.js"></script>
                    <!-- endbuild -->
                </body>
            </html>
            
        Bloc après injection :
        
        *./public_html/index.html*
        
                    <!-- build:js assets/js/main.min.js -->
                    <!-- bower:js -->
                    <script src="bower_components/jquery/dist/jquery.js"></script>
                    <!-- endbower -->
                    <script src="assets/js/main.js"></script>
                    <!-- endbuild -->
                </body>
            </html>

* **Tâche "dev:fonts"**

    *./gulp.js*
    
        gulp.task('dev:fonts', function (cb) {
            var bowerFontsFiles = mainBowerFiles({
                includeDev: true,
                filter: '**/*.{otf,eot,svg,ttf,woff,woff2}'
            });
            // check for glob empty array
            if (bowerFontsFiles.length === 0) {
                cb();
                return;
            }

            return gulp.src(bowerFontsFiles, {allowEmpty: true})
                    .pipe(gulp.dest(paths.fonts.src));
        });
    
    * Description des actions :
        
        - Récupération des fonts des composants Bower à partir de la variable **mainBowerFiles** précédemment initialisée. Les fonts sont copiées dans le répertoire fonts source

* **Tâche "prod:useref"**

    *./gulp.js*
    
        gulp.task('prod:useref', function () {
            return gulp
                    .src(devFiles.html)
                    .pipe(plugins.useref())
                    .pipe(plugins.if('*.js', plugins.uglify()))
                    .pipe(plugins.if('*.js', plugins.rev()))
                    .pipe(plugins.if('*.css', plugins.cleanCss()))
                    .pipe(plugins.if('*.css', plugins.rev()))
                    .pipe(plugins.revReplace({replaceInExtensions: ['.html']}))
                    .pipe(gulp.dest(basePaths.dest))
                    .pipe(plugins.rev.manifest('rev-manifest.json', {merge: true}))
                    .pipe(gulp.dest(basePaths.dest + assetsFolder));
        });
    
    * Description des actions
        
        - Concaténation(plugin gulp-useref) des fichiers de scripts déclarés dans un bloc js (commentaires build:js) dans le fichier "index.html"
        - Minification des scripts (plugin gulp-uglify)
        - Versioning (plugin gul-rev gulp-rev-replace) du fichier final de scripts "main.min.js" (ajout d'un prefixe de version au nom du fichier)
        - Enregistrement du fichier "main[-version].min.js" dans le répertoire js distant.
        
        *./public_html/index.html avant concaténation et versioning des scripts*
        
                    <!-- build:js assets/js/main.min.js -->
                    <!-- bower:js -->
                    <script src="bower_components/jquery/dist/jquery.js"></script>
                    <!-- endbower -->
                    <script src="assets/js/main.js"></script>
                    <!-- endbuild -->
                </body>
            </html>
        
        *./public_html/index.html avant concaténation*
        
                    <script src="assets/js/main-c8681f5bfa.min.js"></script>
                </body>
            </html>
        
        - Concaténation (plugin gulp-useref) des fichiers de styles déclarés dans un bloc css (commentaires build:css) dans le fichier "index.html"
        - Minification des styles (plugin gulp-clean-css)
        - Versioning (plugin gul-rev gulp-rev-replace) du fichier final de style "main.min.css" (ajout d'un prefixe de version au nom du fichier)
        - Enregistrement du fichier "main[-version].min.css" dans le répertoire css distant
        
        *./public_html/index.html avant concaténation et versioning des styles*
        
            <!doctype html>
            <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Netbeans gulp</title>
                    <!-- build:css assets/css/main.min.css -->
                    <link href="assets/css/main.css" rel="stylesheet">
                    <!-- endbuild -->
                </head>
        
        *./public_html/index.html après concaténation et versioning des styles*
        
            <!doctype html>
            <html lang="fr">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Netbeans gulp</title>
                    <link rel="stylesheet" href="assets/css/main-56ee78032f.min.css">
                </head>
        
        - Enregistrement des versions dans un fichier manifest dans le répertoire des assets distant
        - Copie des fichiers html dans le répertoire distant.

* **Tâche "prod:fonts"**

    *./gulp.js*
    
        gulp.task('prod:fonts', function () {
            return gulp
                    .src(devFiles.fonts)
                    .pipe(gulp.dest(paths.fonts.dest));
        });

    * Description des actions :
        
        - Copie des fonts du répertoire source vers le répertoire distant

* **Tâche "prod:images"**

    *./gulp.js*
    
        gulp.task('prod:images', function () {
            return gulp.src(devFiles.images)
                    .pipe(plugins.cache(
                        plugins.imagemin({
                            verbose: true
                        })
                    ))
                    .pipe(gulp.dest(paths.images.dest));
        });

    * Description des actions :
        
        - Compression des images (plugin gulp-imagemin)
        - Mise en cache (plugin gulp-cache) des tâches de compression pour ne pas les répéter lors des prochains build
        - Copie des images du répertoire source vers le répertoire distant

* **Tâche "browserSync:init"**

    *./gulp.js*
    
        gulp.task('browserSync:init', function () {
            browserSync.init({
                server: {
                    baseDir: basePaths.src
                },
                browser: browsers,
                notify: false
            });
        });

    * Description des actions :
        
        - Initialise une instance de browsersync (plugin browser-sync) pour tests dans le ou les navigateurs déclarés dans la variable "browsers"

* **Tâche "browserSync:reload"**

    *./gulp.js*
    
        gulp.task('browserSync:reload', function (done) {
            browserSync.reload();
            done();
        });

    * Description des actions :
        
        - Rafraichissement des navigateurs de l'instance browsersync

* **Tâche "clearCache"**

    *./gulp.js*
    
        gulp.task('clearCache', function (done) {
            return plugins.cache.clearAll(done);
        });

    * Description des actions :
        
        - Suppression (plugin gulp-cache) si nécessaire du cache généré lors de la compression des images

* **Tâche "clean"**

    *./gulp.js*
    
        gulp.task('clean', function (done) {
            del.sync('dist/**/*', done());
        });

    * Description des actions :
        
        - Suppression du contenu du répertoire distant avant build

* **Tâche "build"**

    *./gulp.js*
    
        gulp.task('build', gulp.series(
                'clean', 
                gulp.parallel('dev:styles', 'dev:scripts', 'dev:fonts'),
                gulp.parallel('prod:useref', 'prod:images', 'prod:fonts')
        ));

    * Description des actions :
        
        - Exécution séquentielle des tâches de nettoyage, de développement et de production

* **Tâche "watch"**

    *./gulp.js*
    
        gulp.task('watch', function () {
            // styles
            gulp.watch(devFiles.scss, gulp.series('dev:styles', 'browserSync:reload'));
            // js
            gulp.watch(devFiles.scripts, gulp.parallel('browserSync:reload'));
            // html
            gulp.watch(devFiles.html, gulp.parallel('browserSync:reload'));
            // img
            gulp.watch(devFiles.images, gulp.parallel('browserSync:reload'));
        });

    * Description des actions :
        
        - Surveillance des modifications des fichiers html et des assets dans le répertoire source
        - Rafraîchissement du navigateur
        - Compilation Sass (tâche dev:styles) lors de modification des fichiers scss

* **Tâche "default"**

    *./gulp.js*
    
        gulp.task('default', gulp.series(
                gulp.parallel('dev:styles', 'dev:scripts', 'dev:fonts'),
                gulp.parallel('browserSync:init', 'watch')
        ));

    * Description des actions :
        
        - Initialise le développement (tâche de dév)
        - Instancie browsersync
        - Lance le watcher

### 6. Exemple de paramétrage des tâches Gulp dans Netbeans

![netbeans gulp options](../img/03_Netbeans_projet_taches.jpg)
